import { BhmfrontPage } from './app.po';

describe('bhmfront App', () => {
  let page: BhmfrontPage;

  beforeEach(() => {
    page = new BhmfrontPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
