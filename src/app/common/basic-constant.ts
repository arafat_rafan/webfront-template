export class BasicConstant{
    /**
     * Constant Message
     */
    static httpGet = 'httpDataByGet';
    static httpPost = 'httpDataByPost';
    static httpPut = 'httpDataByPut';
    /**
     * Constant variable
     */
    static sCode = 'Status Code: ';
    static sMessages = 'Stattus Message: ';
    private static urlInitial = 'http://';
    private static serverAddress = BasicConstant.urlInitial + 'localhost';
    private static serverAddressWithPort = BasicConstant.serverAddress + ':8080';
    private static separator = '/';
    private static apiInitial = '/bhm';
    private static authAPI = BasicConstant.apiInitial + '/auth';
    /**
     * Local Location
     */
    static basicResourceFileURI = '/resources/data/basic.json';
    static credentialResourceFileURI = '/resources/data/credential/basic.json';
    static headerDataResourceFileURI = '/resources/data/header-data.json';
    static demoDataFileURI = '/resources/data/inspection-form-data.json';
    /**
     * API Location
     */
    static signupURI = BasicConstant.serverAddressWithPort + BasicConstant.authAPI + BasicConstant.separator + 'signup';
    static loginURI = BasicConstant.serverAddressWithPort + BasicConstant.authAPI + BasicConstant.separator + 'login';
}
