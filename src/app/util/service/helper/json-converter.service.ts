import { Injectable } from '@angular/core';
import { JsonConvert, OperationMode, ValueCheckingMode } from 'json2typescript';

/**
 * Models / Interfaces
 */

/**
 * Services
 */
import { LogHandlerService } from '../helper/log-handler.service';
/**
 * Miscellaneous
 */

@Injectable()
export class JsonConverterService {

    constructor(private logHandlerService: LogHandlerService) {
    }

    convertJson(jsonObject: Object, model: new () => Object): Object {
        let jsonConvert: JsonConvert = new JsonConvert();

        jsonConvert.operationMode = this.logHandlerService.printJsonConversionLog();
        jsonConvert.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
        jsonConvert.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null

        model = jsonConvert.deserialize(jsonObject, model);
        return model;
    }
}