/**
 * SingleTon Helper
 * Added for Future Use
 */
import { LogBHM } from '../../model/common/log-bhm';
export class LogBHMHandler {
    //Avoid Lazy Loading Initialization
    //private static singleInstance: LogBHMHandler = new LogBHMHandler();
    private static singleInstance: LogBHMHandler;
    separator = " - ";

    getClassInfo(): string {
        return LogBHMHandler.name;
    }
    constructor() {
        if (LogBHMHandler.singleInstance) {
            this.printLogBHM(this.getClassInfo(), "LogBHM: Instantiation failed: Use LogBHMHandler.getInstance() instead of new.");
        }
        //LogBHMHandler.singleInstance = this;
    }
    public static getInstance(): LogBHMHandler {
        if (!LogBHMHandler.singleInstance){
            LogBHMHandler.singleInstance = new LogBHMHandler();
        }
        return LogBHMHandler.singleInstance;
    }
    public printLogBHM(className: string, messages: string): void {
        throw new Error(className + this.separator + messages);
    }
    public printLog(className: string, messages: string): void {
        console.log(className + this.separator + messages);
    }
    public printWarn(className: string, messages: string): void {
        console.warn(className + this.separator + messages);
    }
}