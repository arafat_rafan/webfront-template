import { Injectable } from '@angular/core';
import { OperationMode } from 'json2typescript';

/**
 * Models / Interfaces
 */

/**
 * Services
 */
import { BasicConfiguration } from '../common/basic-configuration.service';
/**
 * Miscellaneous
 */
import { LogBHM } from '../../model/common/log-bhm';

@Injectable()
export class LogHandlerService implements LogBHM {
    separator = " - ";

    constructor(private basicConfiguration: BasicConfiguration) { }

    public printError(className: string, where: string, messages: any): void {
        if (this.basicConfiguration.isErrorLogEnable()) {
            console.error(className + this.separator + where + this.separator + messages);
            console.log(messages);
        }
    }
    public printLog(className: string, where: string, messages: any): void {
        if (this.basicConfiguration.isLogEnable()) {
            console.log(className + this.separator + where + this.separator + messages);
            console.log(messages);
        }
    }
    public printWarn(className: string, where: string, messages: any): void {
        if (this.basicConfiguration.isWarnLogEnable()) {
            console.warn(className + this.separator + where + this.separator + messages);
            console.log(messages);
        }
    }
    public printJsonConversionLog(): number {
        if (this.basicConfiguration.isLogEnable()) {
            return OperationMode.LOGGING;
        }
        return OperationMode.DISABLE;
    }
}