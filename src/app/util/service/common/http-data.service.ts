import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
/**
 * Models / Interfaces
 */
import {ErrorHandler} from '../../model/common/error-handler';
/**
 * Services
 */
import {LogHandlerService} from '../helper/log-handler.service';
/**
 * Miscellaneous
 */
import {BasicConstant} from '../../../common/basic-constant';
import {DataConversionType} from '../../enum/data-conversion-type';


@Injectable()
export class HTTPDataService implements ErrorHandler {
  constructor(private http: Http, private logService: LogHandlerService) {
  }

  getHeader(): Headers {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    return headers;

  }

  getRequestOptionsWithAuthorization(): any {
    const headers = this.getHeader();
    headers.append('Authorization', localStorage.getItem('apiAuth'));
    return new RequestOptions({headers: headers});
  };

  getRequestOptionsWithoutAuthorization(): any {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    return new RequestOptions({headers: headers});
  };

  getClassInfo(): string {
    return HTTPDataService.name;
  }

  public httpDataByGet(location: string): Observable<any> {
    return this.http.get(location, this.getRequestOptionsWithAuthorization())
      .map((res: Response) => {
        if (res.status < 200 || res.status > 300) {
          this.logService.printError(this.getClassInfo(), BasicConstant.httpGet, res.status + BasicConstant.sMessages + res.statusText);
        } else {
          this.logService.printLog(this.getClassInfo(), BasicConstant.httpGet, res.toString());
          return this.responseHandler(res, DataConversionType.json);
        }
      })
      .catch(this.handleError);
  }

  public httpDataByPost(location: string, data: any): Observable<any> {
    return this.http.post(location, JSON.stringify(data), this.getRequestOptionsWithAuthorization())
      .map((res: Response) => {
        if (res.status < 200 || res.status > 300) {
          this.logService.printError(this.getClassInfo(), BasicConstant.httpPost, res.status + BasicConstant.sMessages + res.statusText);
        } else {
          this.logService.printLog(this.getClassInfo(), BasicConstant.httpPost, res.toString());
          return this.responseHandler(res, DataConversionType.json);
        }
      })
      .catch(this.handleError);
  }

  public httpDataByPut(location: string, data: any): Observable<any> {
    return this.http.put(location, JSON.stringify(data), this.getRequestOptionsWithAuthorization())
      .map((res: Response) => {
        if (res.status < 200 || res.status > 300) {
          this.logService.printError(this.getClassInfo(), BasicConstant.httpPut, res.status + BasicConstant.sMessages + res.statusText);
        } else {
          this.logService.printLog(this.getClassInfo(), BasicConstant.httpPut, res.toString());
          return this.responseHandler(res, DataConversionType.json);
        }
      })
      .catch(this.handleError);
  }

  public handleError(error: any) {
    return Observable.throw(error);
  }

  public responseHandler(data: any, responseReturnType: DataConversionType): any {
    if (responseReturnType === DataConversionType.json) {
      /* return JSON.stringify(data.json()); */
      return data.json();
    }
    return data;
  }
}
