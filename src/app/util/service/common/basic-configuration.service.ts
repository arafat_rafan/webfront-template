import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
/**
 * Models / Interfaces
 */

/**
 * Services
 */

/**
 * Miscellaneous
 */

@Injectable()
export class BasicConfiguration {

    isLogEnable(): boolean {
        return true;
    }
    isErrorLogEnable(): boolean {
        return true;
    }
    isWarnLogEnable(): boolean {
        return true;
    }
}
