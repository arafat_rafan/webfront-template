export interface ResponseObserver {
    response(data: any);
}
