export interface ErrorHandler {
    handleError(error: Error): any;
}
