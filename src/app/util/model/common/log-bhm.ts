export interface LogBHM {
    printError(className: string, where: string, messages: any): void;
    printLog(className: string, where: string, messages: any): void;
    printWarn(className: string, where: string, messages: any): void;
    printJsonConversionLog(): number;
}
