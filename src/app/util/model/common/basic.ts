import { JsonObject, JsonProperty } from 'json2typescript';
/**
 * Models / Interfaces
 */

/**
 * Services
 */

/**
 * Miscellaneous
 */
@JsonObject
export class Basic {
    @JsonProperty('separator', String)
    separator = '-';
    @JsonProperty('companyName', String)
    companyName: string = undefined;
    @JsonProperty('appName', String)
    appName: string;
    @JsonProperty('mainRouting', [String])
    mainRouting: string[];
    // constructor(options: {
    //     separator?: string,
    //     companyName?: string,
    //     appName?: string
    //     mainRouting?:string[]
    //   } = {}) {
    //   this.separator = options.separator || '';
    //   this.companyName = options.companyName || '';
    //   this.appName = options.appName || '';
    //   this.mainRouting = options.mainRouting != null && options.mainRouting.length > 0 ? options.mainRouting : null;
    // }
}
