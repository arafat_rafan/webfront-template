import {Injectable} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
/**
 * Models / Interfaces
 */
import {FormData} from '../form/form-data';
/**
 * Services
 */
import {LogHandlerService} from '../../util/service/helper/log-handler.service';
/**
 * Miscellaneous
 */
import {FormHelper} from '../../ui/helper/form-helper';
import {FormFieldType} from '../enum/form-field-type';

@Injectable()
export class UIControlService {
  formHelper: FormHelper;

  constructor(private logHandlerService: LogHandlerService) {
    this.formHelper = new FormHelper(logHandlerService);
  }

  getClassInfo(): string {
    return UIControlService.name;
  }

  toFormGroup(datas: FormData<any>[]) {
    this.logHandlerService.printLog(this.getClassInfo(), 'toFormGroup', datas);
    const group: any = {};
    try {
      datas.sort((a, b) => a.order - b.order);
      datas.forEach(data => {
        this.logHandlerService.printLog(this.getClassInfo(), 'toFormGroup', data.key);
        const validators: any[] = this.formHelper.getValidator(data.validator);
        if (data['type'] === FormFieldType.checkbox) {
          group[data.key] = new FormControl(data['checked'] || false, Validators.compose(
            validators));
        }else {
          group[data.key] = new FormControl(data.value || '', Validators.compose(
            validators));
        }

      });
      this.logHandlerService.printLog(this.getClassInfo(), 'toFormGroup', group);
    } catch (error) {
      this.logHandlerService.printError(this.getClassInfo(), 'toFormGroup', error);
    }
    return new FormGroup(group);
  }

  toFormBuilder(formGroup: FormGroup) {

  }

  toFormArray(formGroups: FormGroup[]) {

  }
}
