import { Validators } from '@angular/forms';

/**
 * Models / Interfaces
 */

/**
 * Services
 */
import { LogHandlerService } from '../../util/service/helper/log-handler.service'
/**
 * Miscellaneous
 */
import { FormValidatorType } from '../enum/form-validator';

export class FormHelper {
  lineEnd = ' .'
  lineSpace = ' ';
    constructor(private logHandlerService: LogHandlerService) { }
    getClassInfo(): string {
        return FormHelper.name;
    }
    getValidator(validators: any): any[] {
        const formValidator: any[] = [];
        try {
            Object.keys(validators).map(key => {
                this.logHandlerService.printLog(this.getClassInfo(), 'getValidator', key);
                switch (key) {
                    case FormValidatorType.required.toString():
                        /*this.logHandlerService.printLog(this.getClassInfo(), "getValidator", "required");*/
                        formValidator.push(Validators.required);
                        break;
                    case FormValidatorType.minLength.toString():
                        // this.logHandlerService.printLog(this.getClassInfo(), "getValidator", "minLength");
                        formValidator.push(Validators.minLength(validators[key]));
                        break;
                    case FormValidatorType.maxLength.toString():
                        // this.logHandlerService.printLog(this.getClassInfo(), "getValidator", "maxLength");
                        formValidator.push(Validators.maxLength(validators[key]));
                        break;
                    case FormValidatorType.min.toString():
                        // this.logHandlerService.printLog(this.getClassInfo(), "getValidator", "min");
                        formValidator.push(Validators.min(validators[key]));
                        break;
                    case FormValidatorType.max.toString():
                        // this.logHandlerService.printLog(this.getClassInfo(), "getValidator", "max");
                        formValidator.push(Validators.max(validators[key]));
                        break;
                    case FormValidatorType.email.toString():
                        // this.logHandlerService.printLog(this.getClassInfo(), "getValidator", "email");
                        formValidator.push(Validators.email);
                        break;
                    default:
                        break;
                }
            });
        } catch (error) {
            this.logHandlerService.printError(this.getClassInfo(), 'getValidator', error);
        }
        return formValidator;
    }
    getErrorMessage(errors: any, label: string): string {
        let errorMessage: string = label + ' validation Failed!';
        try {
            Object.keys(errors).map(key => {
                this.logHandlerService.printLog(this.getClassInfo(), 'getErrorMessage', key);
                switch (key) {
                    case FormValidatorType.required.toString():
                        this.logHandlerService.printLog(this.getClassInfo(), 'getErrorMessage', errors);
                        errorMessage += this.lineSpace + label + ' must be filled up/selected' + this.lineEnd;
                        break;
                    case FormValidatorType.minLength.toString().toLowerCase():
                        this.logHandlerService.printLog(this.getClassInfo(), 'getErrorMessage', errors);
                        errorMessage += this.lineSpace + label + '\'s minimum length is ' + errors.minlength.requiredLength + this.lineEnd;
                        break;
                    case FormValidatorType.maxLength.toString().toLowerCase():
                        this.logHandlerService.printLog(this.getClassInfo(), 'getErrorMessage', errors);
                        errorMessage += this.lineSpace + label + '\'s maximum length is ' + errors.maxlength.requiredLength + this.lineEnd;
                        break;
                    case FormValidatorType.min.toString():
                        this.logHandlerService.printLog(this.getClassInfo(), 'getErrorMessage', errors);
                        errorMessage += this.lineSpace + label + ' must not be less than ' + errors.min.min + this.lineEnd;
                        break;
                    case FormValidatorType.max.toString():
                        this.logHandlerService.printLog(this.getClassInfo(), 'getErrorMessage', errors);
                        errorMessage += this.lineSpace + label + ' only allowed upto ' + errors.max.max + this.lineEnd;
                        break;
                    case FormValidatorType.email.toString():
                        this.logHandlerService.printLog(this.getClassInfo(), 'getErrorMessage', errors);
                        errorMessage += this.lineSpace + label + ' is not a valid email address ' + this.lineEnd;
                        break;
                    default:
                        break;
                }
            });
        } catch (error) {
            this.logHandlerService.printError(this.getClassInfo(), 'getValidator', error);
        }
        return errorMessage;
    }
}
