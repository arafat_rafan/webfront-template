export enum FormFieldType {
    intputbox = <any>'intputbox',
    optionfield = <any>'optionfield',
    textbox = <any>'textbox',
    number = <any>'number',
    email = <any>'email',
    radio = <any>'radio',
    checkbox = <any>'checkbox',
    textarea = <any>'textarea'
}
