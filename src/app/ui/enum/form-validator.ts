export enum FormValidatorType {
    required = <any>'required',
    min = <any>'min',
    max = <any>'max',
    minLength = <any>'minLength',
    maxLength = <any>'maxLength',
    email = <any>'email',
}
