/**
 * Models / Interfaces
 */

/**
 * Services
 */

/**
 * Miscellaneous
 */
import {JsonObject, JsonProperty, Any} from 'json2typescript';

@JsonObject
export class FormData<T> {
  @JsonProperty('key', String)
  key: string;
  @JsonProperty('label', String)
  label: string;
  @JsonProperty('value', Any, true )
  value?: any;
  @JsonProperty('validator', Any, true)
  validator?: any;
  @JsonProperty('order', Number)
  order: number;
  @JsonProperty('controlType', String)
  controlType: string;

  constructor(options: {
    value?: number,
    key?: string,
    label?: string,
    validator?: any,
    order?: number,
    controlType?: string,
  } = {}) {
    this.value = options.value;
    this.key = options.key;
    this.label = options.label;
    this.validator = options.validator;
    this.order = options.order;
    this.controlType = options.controlType;
  }
}
