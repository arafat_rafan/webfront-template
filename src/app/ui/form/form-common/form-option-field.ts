/**
 * Models / Interfaces
 */
import {FormInputBox} from './form-inputbox';
/**
 * Services
 */

/**
 * Miscellaneous
 */
import { JsonObject, JsonProperty, Any } from 'json2typescript';

@JsonObject
export class FormOptionField extends FormInputBox {
    @JsonProperty('options', [Any])
    options: {key: string, value: string}[] = [];
    constructor(options: {} = {}) {
        super(options);
        this.options = options['options'] || [];
    }
}
