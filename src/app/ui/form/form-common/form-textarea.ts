/**
 * Models / Interfaces
 */
import {FormData} from '../form-data';
/**
 * Services
 */

/**
 * Miscellaneous
 */
import { JsonObject, JsonProperty } from 'json2typescript';

@JsonObject
export class FormTextArea extends FormData<string> {
    @JsonProperty('rows', Number)
    rows: string;
    @JsonProperty('cols', Number, true)
    cols?: boolean;
    constructor(options: {} = {}) {
       super(options);
       this.rows = options['rows'] || [];
       this.cols = options['cols'] || [];
    }
}
