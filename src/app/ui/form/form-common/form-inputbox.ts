/**
 * Models / Interfaces
 */
import {FormData} from '../form-data';
/**
 * Services
 */

/**
 * Miscellaneous
 */
import { JsonObject, JsonProperty } from 'json2typescript';

@JsonObject
export class FormInputBox extends FormData<string> {
    @JsonProperty('type', String)
    type: string;
    @JsonProperty('checked', Boolean, true)
    checked?: boolean;
    constructor(options: {} = {}) {
       super(options);
       this.type = options['type'] || [];
       this.checked = options['checked'] || [];
    }
}
