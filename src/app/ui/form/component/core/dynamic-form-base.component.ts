import {Component, Input, OnInit, OnChanges, SimpleChanges} from '@angular/core';
import {FormGroup} from '@angular/forms';
/**
 * Models / Interfaces
 */
import {FormData} from '../../form-data';
/**
 * Services
 */
import {LogHandlerService} from '../../../../util/service/helper/log-handler.service';
import {UIControlService} from '../../../service/ui-control.service';
/**
 * Miscellaneous
 */

import {FormFieldType} from '../../../enum/form-field-type';

@Component({
  selector: 'app-bhm-dynamic-form-base',
  templateUrl: './dynamic-form-base.component.html',
  providers: [UIControlService]
})
export class DynamicFormBaseComponent implements OnInit, OnChanges {
  @Input() formData: FormData<any>[] = [];
  form: FormGroup;
  payLoad = '';

  constructor(private uiControlService: UIControlService, private logHandlerService: LogHandlerService) {
  }

  getClassInfo(): string {
    return DynamicFormBaseComponent.name;
  }

  ngOnInit() {
    this.logHandlerService.printLog(this.getClassInfo(), 'ngOnInit', this.formData);
    this.form = this.uiControlService.toFormGroup(this.formData);
  }

  ngOnChanges(changes: SimpleChanges) {
    // changes.prop contains the old and the new value...
    this.logHandlerService.printLog(this.getClassInfo(), 'ngOnChanges', changes.formData.currentValue);
    this.form = this.uiControlService.toFormGroup(changes.formData.currentValue);
  }

  onSubmit() {
    this.formData.forEach((fieldData) => {
      this.logHandlerService.printLog(this.getClassInfo(), 'onSubmit', fieldData);
      if (fieldData['type'] === FormFieldType.number) {
        const convertedValue = +this.form.value[fieldData.key];
        this.form.value[fieldData.key] = convertedValue;
      }
    });
    this.payLoad = JSON.stringify(this.form.value);
  }
}
