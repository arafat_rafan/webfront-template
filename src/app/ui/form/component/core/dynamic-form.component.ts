import {Component, Input} from '@angular/core';
import {FormGroup} from '@angular/forms';
/**
 * Models / Interfaces
 */
import {FormData} from '../../form-data';
/**
 * Services
 */
import {LogHandlerService} from '../../../../util/service/helper/log-handler.service';
/**
 * Miscellaneous
 */
import {FormHelper} from '../../../helper/form-helper';
import {FormFieldType} from '../../../enum/form-field-type';

@Component({
  selector: 'app-bhm-dynamic-form',
  templateUrl: './dynamic-form.component.html',
})
export class DynamicFormComponent {
  @Input() fieldData: FormData<any>;
  @Input() form: FormGroup;
  formHelper: FormHelper;
  validationMessage: String;

  constructor(private logHandlerService: LogHandlerService) {
    this.formHelper = new FormHelper(logHandlerService);
  }

  getClassInfo(): string {
    return DynamicFormComponent.name;
  }

  get isValid() {
    this.logHandlerService.printLog(this.getClassInfo(), 'isValid', this.form.value);
    if (this.form.controls[this.fieldData.key].touched) {
      const errors = this.form.controls[this.fieldData.key].errors;
      this.logHandlerService.printLog(this.getClassInfo(), 'isValid', this.form.controls[this.fieldData.key]);
      if (errors != null) {
        this.validationMessage = this.formHelper.getErrorMessage(errors, this.fieldData.label);
        this.logHandlerService.printLog(this.getClassInfo(), 'isValid', this.validationMessage);
        return false;
      }
    }
    return true;
  }

  valueUpdate(value: any) {
    // const convertedValue = !event.target.value;
    this.logHandlerService.printLog(this.getClassInfo(), 'valueUpdate', value);
    // this.form.controls[this.fieldData.key].setValue(convertedValue);
    this.form.controls[this.fieldData.key].setValue(value);
  }
  // Invoked When Value Need to update into FormControl
  eventTriggered(event: any) {
    if (this.fieldData['type'] === FormFieldType.checkbox) {
      this.logHandlerService.printLog(this.getClassInfo(), 'eventTriggered', event.target.checked);
      if (event.target.checked) {
        this.valueUpdate(true);
      }else {
        this.valueUpdate('');
      }
    }
  }

  onSelectionChange(value: any) {
    this.valueUpdate(value);
  }
}
