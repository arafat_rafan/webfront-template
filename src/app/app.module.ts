import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
/**
 * Modules
 */
import {InspectionModule} from './inspection/inspection.module';
import {DashboardModule} from './dashboard/dashboard.module';
import { AppRoutingModule } from './app-routing.module';
/**
 * Services
 */
import {HTTPDataService} from './util/service/common/http-data.service';
import {BasicConfiguration} from './util/service/common/basic-configuration.service';
import {LogHandlerService} from './util/service/helper/log-handler.service';
import {JsonConverterService} from './util/service/helper/json-converter.service';
/**
 * Components
 */
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './common/component/not-found.component';


@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    InspectionModule,
    DashboardModule,
    AppRoutingModule
  ],
  providers: [HTTPDataService, BasicConfiguration, LogHandlerService, JsonConverterService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
