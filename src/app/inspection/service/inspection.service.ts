import {Injectable} from '@angular/core';

import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

/**
 * Models / Interfaces
 */
import {ErrorHandler} from '../../util/model/common/error-handler';
import {FormData} from '../../ui/form/form-data';
import {FormInputBox} from '../../ui/form/form-common/form-inputbox';
import {FormTextArea} from '../../ui/form/form-common/form-textarea';
import {FormOptionField} from '../../ui/form/form-common/form-option-field';
/**
 * Services
 */
import {LogHandlerService} from '../../util/service/helper/log-handler.service';
import {HTTPDataService} from '../../util/service/common/http-data.service';
import {JsonConverterService} from '../../util/service/helper/json-converter.service';
/**
 * Miscellaneous
 */
import {DataConversionType} from '../../util/enum/data-conversion-type';
import {BasicConstant} from '../../common/basic-constant';
import {FormFieldType} from '../../ui/enum/form-field-type';

@Injectable()
export class InspectionService implements ErrorHandler {
  constructor(private logService: LogHandlerService, private httpService: HTTPDataService, private jsonService: JsonConverterService) {}

  getClassInfo(): string {
    return InspectionService.name;
  }

  fetchRawData(): Observable<any[]> {
    return this.httpService.httpDataByGet(BasicConstant.demoDataFileURI).map((data) => {
      this.logService.printLog(this.getClassInfo(), 'fetchRawData', data);
      /*return this.responseHandler(data, DataConversionType.object);*/
      return this.responseHandler(data, DataConversionType.object);
    })
      .catch(this.handleError);
  }

  public responseHandler(data: any, responseReturnType: DataConversionType) {
    try {
      const formData: FormData<any>[] = [];
      if (responseReturnType === DataConversionType.object) {
        Object.keys(data).map(key => {
          const fieldInfo = data[key];
          let fieldData;
          if (fieldInfo.controlType === FormFieldType.intputbox) {
            fieldData = this.jsonService.convertJson(fieldInfo, FormInputBox);
          } else if (fieldInfo.controlType === FormFieldType.optionfield) {
            fieldData = this.jsonService.convertJson(fieldInfo, FormOptionField);
          } else if (fieldInfo.controlType === FormFieldType.textarea) {
            fieldData = this.jsonService.convertJson(fieldInfo, FormTextArea);
          }
          /*this.logHandlerService.printLog(this.getClassInfo(), "response", fieldData);*/
          formData.push(fieldData);
        });
      }
      // return formData.sort((a, b) => a.order - b.order);
      return formData;
    } catch (error) {
      this.logService.printError(this.getClassInfo(), 'response', error);
    }
    /*return data;*/
  }

  public handleError(error: any) {
    return Observable.throw(error);
  }
}
