import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Rx';
/**
 * Models / Interfaces
 */
import { FormData } from '../../ui/form/form-data'
/**
 * Services
 */
import { LogHandlerService } from '../../util/service/helper/log-handler.service';
import { InspectionService } from '../service/inspection.service';
/**
 * Miscellaneous
 */
import { BasicConstant } from '../../common/basic-constant';

@Component({
  templateUrl: './inspection.component.html'
})
export class InspectionComponent implements OnInit{
  formData: any[] = [];
  constructor(private logHandlerService: LogHandlerService, private inspectionService: InspectionService) {
    // this.getInspectionFormData();
    /* this.inspectionService.setObserver(this); */

  }

  getClassInfo(): string {
    return InspectionComponent.name;
  }
  getInspectionFormData() {
     this.inspectionService.fetchRawData().subscribe(
      (data) => {
        this.formData = data;
        this.logHandlerService.printLog(this.getClassInfo(), "getInspectionFormData ", this.formData);
      }
    );
  }

  ngOnInit() {
    this.getInspectionFormData();
    /* let timer = Observable.timer(2000, 5000);
    timer.subscribe(() => this.getInspectionFormData()); */
}
}
