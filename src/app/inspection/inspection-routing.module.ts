import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
/**
 * Components
 */
import {InspectionComponent} from './component/inspection.component';
import {ReportComponent} from './report/component/report.component';
import {StatisticsComponent} from './statistics/component/statistics.component';


const inspectionRoutes: Routes = [
  {
    path: 'inspection',
    component: InspectionComponent,
    children: [
      {
        path: 'report/:id',
        component: ReportComponent
      },
      {
        path: 'statistics',
        component: StatisticsComponent
      },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(
      inspectionRoutes
    )
  ],
  exports: [
    RouterModule
  ]
})
export class InsppectionRoutingModule { }
