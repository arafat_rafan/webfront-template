import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
/**
 * Modules
 */
import {InsppectionRoutingModule} from './inspection-routing.module';
import {ReportModule} from './report/report.module';
import {StatisticsModule} from './statistics/statistics.module';
/**
 * Components
 */
import {InspectionComponent} from './component/inspection.component';
import {DynamicFormBaseComponent} from '../ui/form/component/core/dynamic-form-base.component';
import {DynamicFormComponent} from '../ui/form/component/core/dynamic-form.component';
/**
 * Services
 */
import {InspectionService} from './service/inspection.service';

@NgModule({
  declarations: [
    InspectionComponent,
    DynamicFormBaseComponent,
    DynamicFormComponent
  ],
  imports: [
    ReportModule,
    StatisticsModule,
    InsppectionRoutingModule,
    ReactiveFormsModule,
    CommonModule
  ],
  providers: [InspectionService]
})
export class InspectionModule {
}
