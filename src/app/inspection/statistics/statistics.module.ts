import { NgModule } from '@angular/core';

import {StatisticsRoutingModule} from './statistics-routing.module';

import {StatisticsComponent} from './component/statistics.component';

@NgModule({
  declarations: [
    StatisticsComponent
  ],
  imports: [
    StatisticsRoutingModule
  ],
  providers: [],
})
export class StatisticsModule {
}
