import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {StatisticsComponent} from './component/statistics.component';

const statisticsRoutes: Routes = [
  {
    path: 'statistics',
    component: StatisticsComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(
      statisticsRoutes
    )
  ],
  exports: [
    RouterModule
  ]
})
export class StatisticsRoutingModule { }
