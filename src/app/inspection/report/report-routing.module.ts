import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {ReportComponent} from './component/report.component';


const reportRoutes: Routes = [
  {
    path: 'report',
    component: ReportComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(
      reportRoutes
    )
  ],
  exports: [
    RouterModule
  ]
})
export class ReportRoutingModule { }
