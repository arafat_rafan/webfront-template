import { NgModule } from '@angular/core';

import {ReportComponent} from './component/report.component';

import {ReportRoutingModule} from './report-routing.module';


@NgModule({
  declarations: [
    ReportComponent
  ],
  imports: [
    ReportRoutingModule
  ],
  providers: [],
})
export class ReportModule {
 }
