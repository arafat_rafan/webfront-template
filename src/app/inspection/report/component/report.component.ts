import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Component({
  templateUrl: './report.component.html'
})
export class ReportComponent {

  constructor(private activatedRoute: ActivatedRoute) { }
}
