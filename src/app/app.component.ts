import { Component, OnInit } from '@angular/core';
/**
 * Services
 */
import { HTTPDataService } from './util/service/common/http-data.service';
import { JsonConverterService } from './util/service/helper/json-converter.service';
/**
 * Miscellaneous
 */
import { BasicConstant } from './common/basic-constant';
import { LogHandlerService } from './util/service/helper/log-handler.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  appName = 'APP';
  constructor(private httpService: HTTPDataService, private logService: LogHandlerService, private jsonService: JsonConverterService) {}
  ngOnInit() {
    // Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    // Add 'implements OnInit' to the class.
    this.setAppBasic();
  }
  getClassInfo(): string {
    return AppComponent.name;
  }
  setAppBasic(): void {
    this.httpService.httpDataByGet(BasicConstant.basicResourceFileURI).subscribe(
      data => {
        this.appName = data != null ? data.appName : this.appName;
      }
    );
     /*let data = {
      "userName": "arafat92040",
      "password": "arafat"
    }*/
    /*this.httpService.httpDataByPost(BasicConstant.signupURI, data).subscribe(
      data => {
        // this.logService.printLog(this.getClassInfo(), "setAppBasic", data);
      },
      (err) => {
        if (err.error instanceof Error) {
          // A client-side or network error occurred. Handle it accordingly.
          this.logService.printLog(this.getClassInfo(), 'setAppBasic', 'An error occurred:' + err.error.message);
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          this.logService.printLog(this.getClassInfo(), 'setAppBasic', `status ${err.status},error: ${err.error}`);
        }
      }
    );*/
  }

}
