import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
/**
 * Components
 */
import {DashboardComponent} from './component/dashboard.component';


const dashBoardRoutes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(
      dashBoardRoutes
    )
  ],
  exports: [
    RouterModule
  ],
  providers: [
  ]
})
export class DashboardRoutingModule { }
