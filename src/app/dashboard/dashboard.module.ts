import { NgModule } from '@angular/core';
/**
 * Modules
 */
import { DashboardRoutingModule } from './dashboard-routing.module';
/**
 * Components
 */
import {DashboardComponent} from './component/dashboard.component';



@NgModule({
  declarations: [
      DashboardComponent
  ],
  imports: [
    DashboardRoutingModule
  ],
  providers: [],
})
export class DashboardModule {
  
 }
